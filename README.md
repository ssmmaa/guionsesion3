[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Guión de Prácticas
## 3. Protocolos de comunicación entre agentes

En el guión se presentará un ejemplo del uso los protocolos [FIPA](http://www.fipa.org/repository/standardspecs.html) y las clases que lo implementan en la biblioteca [Jade](https://jade.tilab.com/doc/api/jade/proto/package-summary.html). Se utilizarán los mismos agentes presentados en el guión de la [Sesión 2](https://gitlab.com/ssmmaa/guionsesion2) presentando las modificaciones necesarias para el diseño de las tareas que afectarán a la comunicación entre los agentes. No se utilizará todos tipos de protocolos que la biblioteca **Jade** implementa, pero el procedimientos es similar con todos ellos.

Para ejemplificar el uso de los protocolos **FIPA** en **Jade** deberemos definir dos clases, una clase que herede de la clase que representará el **rol del iniciador** en el protocolo y otra que represente el **rol del participante** en el protocolo. Las clases que implementan los protocolos en **Jade** que serán las que más utilizaremos en las prácticas de la asignatura son: 

| Protocolo | Clase para el Iniciador | Clase para el Participante |
|-----------|-------------------------| -------------------------- |
| FIPA-Request | [`AchieveREInitiator`](https://jade.tilab.com/doc/api/jade/proto/AchieveREInitiator.html) | [`AchieveREResponder`](https://jade.tilab.com/doc/api/jade/proto/AchieveREResponder.html) |
| FIPA-Propose | [`ProposeInitiator`](https://jade.tilab.com/doc/api/jade/proto/ProposeInitiator.html) | [`ProposeResponder`](https://jade.tilab.com/doc/api/jade/proto/ProposeInitiator.html) |
| FIPA-Contract-Net | [`ContractNetInitiator`](https://jade.tilab.com/doc/api/jade/proto/ContractNetInitiator.html) | [`ContractNetResponder`](https://jade.tilab.com/doc/api/jade/proto/ContractNetResponder.html) |
| FIPA-Subscribe | [`SubscriptionInitiator`](https://jade.tilab.com/doc/api/jade/proto/SubscriptionInitiator.html) | [`SubscriptionResponder`](https://jade.tilab.com/doc/api/jade/proto/SubscriptionResponder.html) |


Para ejemplificar el protocolo **FIPA** lo primero que se mostrará será el esquema *AUML*, un diagrama de secuencia modificado. De esta forma tendremos presentes los mensajes que se intercambiarán entre el **agente iniciador** y los **agentes participantes**, haciendo uso de aquellos que sean útiles para resolver nuestro problema. En **Jade** se tiene una implementación neutra para estos protocolos y ese es el motivo para personalizar las clases y adaptarlas a las necesidades de nuestros problemas.

Cuando se tenga que preparar una documentación de diseño para un problema de agentes, en el que necesitemos el uso de algún protocolo **FIPA** en su resolución, se deberá preparar el diagrama **AUML** adaptado. Es decir, deberá indicarse el contenido del mensaje para que pueda entender si se está utilizando el protocolo de forma correcta. Posteriormente, este contenido, tendrá que venir expresado por elementos de la [ontología](https://es.wikipedia.org/wiki/Ontolog%C3%ADa_%28inform%C3%A1tica%29) de comunicación que se esté utilizando en el problema. 

Se presentará más adelante, en las clases de teoría, los elementos necesarios para el diseño e implementación de una ontología que permita representar la información necesaria para la resolución de los problemas que se pedirán en las prácticas. Para este ejemplo utilizaremos [**Gson**](https://sites.google.com/site/gson/Home), una implementación **JSON** para Java, y así el contenido del mensaje será homogéneo para todos los mensajes y podrá ser interpretado por todos ellos. No es un sustituto adecuado en vez de diseñar una ontología apropiada, pero será un primer paso para mostrar la utilidad de la comunicación entre los agentes.

## 3.1 Protocolos necesarios para nuestro problema

En el ejemplo presentado en el guión de la [Sesión 2](%28https://gitlab.com/ssmmaa/guionsesion2) las necesidades de comunicación entre los agentes implicados son los siguientes:

1. El `AgenteFormulario` envía un `Punto2D` a los `AgenteOperacion` que conoce.
2. El `AgenteFormulario` envía al `AgenteConsola` información relativa a las solicitudes de operaciones solicitadas.
3. El `AgenteOperacion` envía al `AgenteConsola` información relativa al resultado de las operaciones solicitadas.
4. El `AgenteOperacion` debe recibir del `AgenteFormulario` solicitudes de operaciones que debe realizar.
5. El `AgenteConsola` debe recibir de diferentes agentes, tanto `AgenteFormulario` como `AgenteOperacion`, información que presentará en una ventana para que el usuario pueda consultarla.

Ese intercambio de mensajes se han resuelto de forma individual pero hay un diseño más apropiado si utilizamos alguno de los protocolos definidos en **FIPA**. Para este ejemplo utilizaremos los siguientes:

 1. Protocolo **FIPA-Request** para la solicitud de operación entre el `AgenteFormulario`, rol iniciador, y los `AgenteOperacion`, rol participante. 
 2. Protocolo **FIPA-Subscribe** para el envío al `AgenteConsola` de la información sobre las actividades de `AgenteFormulario` y `AgenteOperacion`.

### 3.1.1 Protocolo FIPA-Request

Para la resolución de una operación sobre un Punto2D, que es lo que utilizamos en nuestro ejemplo, utilizaremos el protocolo **FIPA-Request** adaptado a nuestras necesidades quedando de esta forma:

#### TareaRealizarOperacion
```mermaid
sequenceDiagram
	participant i as AgenteFormulario 
	participant r as AgenteOperacion
	i->>r: REQUEST (Punto2D) 
	Note right of r: Petición de una<br/>operación
	alt desconocido
		r-->>i: NOT_UNDERSTOOD (Respuesta)
    else rechazo
	    r-->>i: REFUSE (Respuesta)
	else conforme
		r-->>i: AGREE (Respuesta)
	end
	Note right of r: Resolución de la<br/>operación
	alt no definida
		r->>i: FAILURE (Respuesta)
	else completada
		r->>i: INFORM (Respuesta)
	end
```

En el diagrama se muestra el intercambio de mensajes, indicando el origen y destino, de los agentes implicados. El contenido del mensaje viene representado por la clase de información que lo representa. El diagrama representa tanto al iniciador como al participante, pero para su implementación deberemos tener una tarea para el iniciador y otra para el participante.

Definimos la tarea dentro del paquete `tareas`, y podrá ser utilizada por otros tipos de agentes, vamos a necesitar una interface para poder intercambiar información entre el agente y la tarea. Esta será una interface genérica que se podrá utilizar para cualquier tarea que implique el paso de mensajes **ACL** entre agentes **FIPA**. Para el ejemplo definiremos una interface:

```java
/**
 * Interface definida para las tareas que estén diseñadas en generar mensajes
 * que se presenten posteriormente en una ventana que cumpla con las cracterísticas
 * de una consola para un agente.
 * @author pedroj
 */
public interface Consola<T> {
    public void setMensaje(T msg);
}
```

Con esta interface podremos comunicara al agente, que tenga esta tarea, el objeto que contendrá la información que se genere durante la ejecución de la tarea.

### TareaRealizarOperacionIniciador

Esta tarea corresponderá al `AgenteFormulario` para resolver el rol iniciador del protocolo **FIPA-Request** y para ello debemos personalizar la clase `AchieveREInitiator`. En el paquete `es.uja.ssmmaa.guionsesion3.tareas` definiremos esta clase:

```java
/**
 * Protocolo Request para el rol iniciador que solicitará la realización de una
 * operación definida en el enumerado Operacion para un objeto Punto2D.
 * Se generan mensajes del tipo MensajeConsola con el resultado que se genera en
 * la tarea.
 * @author pedroj
 */
public class TareaRealizarOperacionIniciador extends AchieveREInitiator {
    private final Consola<MensajeConsola> agente;
    private Respuesta msgInfo;
    private final GsonUtil<Respuesta> gsonUtil;
    private AID opAgent;
    
    public TareaRealizarOperacionIniciador(Agent a, ACLMessage msg) {
        super(a, msg);
        
        this.agente = (Consola) a;
        this.gsonUtil = new GsonUtil();
        this.msgInfo = null;
        this.opAgent = null;
    }
...
```

El siguiente paso es implementar los métodos necesarios para los mensajes que el agente debe entender del protocolo. Todos los métodos de la clase `AchieveREInitiator` tienen una implementación neutra. Podemos definir los métodos individuales para cada uno de los tipos de mensajes. La utilización de esta tarea implica que los `AgenteOperacion` deben responder en un tiempo máximo establecido, eso se mostrará más adelante, para evitar que el `AgenteFormulario` quede bloqueado indefinidamente. Esto implica que podemos tratar los mensajes individualmente o esperar a recoger las respuestas de todos y tratarlas a la misma ves. El diseño propuesto de la tarea seguirá esta segunda opción porque así centralizamos en dos métodos el intercambio de mensajes. Además tenemos que implementar un método que permita tratar mensajes que no estén dentro de la secuencia normal de ejecución de la tarea para evitar un mal funcionamiento y permita continuar la ejecución del `AgenteFormulario`.

#### Respuesta a la solicitud de la operación

 Lo primero que tendrá que considerar el `AgenteFormulario` es la respuesta los `AgenteOperacion` a su solicitud. Las opciones que se van ha considerar son las siguientes:

- `NOT_UNDERSTOOD`: En este ejemplo solo consideraremos esta opción si en el contenido del mensaje de la solicitud era nulo.
- `REFUSE`: El `AgenteOperacion` rechaza la realización de la operación. En el contenido del mensaje está la justificación.
- `AGREE`: El `AgenteOperación` realizará la operación y el `AgenteFormulario` espera a su resultado.

El método asociado que se ha implementado es `handleAllResponses(Vector responses)`. Este método se activará cuando todas las respuestas se reciban o el `TIME_OUT`, establecido al crear la tarea, se cumpla. El procedimiento para tratar los mensajes **ACL** tendrá la siguiente estructura:

```java
...
@Override
protected void handleAllResponses(Vector responses) {
    Iterator it = responses.iterator();
        
    while (it.hasNext()) {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        ACLMessage msg = (ACLMessage) it.next();
        opAgent = msg.getSender();
            
        try { 
            msgInfo = gsonUtil.decode(msg.getContent(), Respuesta.class);
            
            switch ( msg.getPerformative() ) {...
```

Se recorrerá toda la estructura de los mensajes y se procesarán uno a uno. En este ejemplo no he tenido en cuenta si alguno de los `AgenteOperacion` no ha llegado a responder. Esto no siempre será así y puede que sea importante que todos los agentes respondan para que se pueda completar exitosamente la tarea. 

Otra cosa que quiero hacer notar es que debemos comprobar siempre si el contenido del mensaje es el esperado, es decir, si se puede producir alguna excepción en la **descodificación** del mensaje. Para el ejemplo, si el contenido del mensaje no corresponde con un objeto Respuesta se generará una excepción y supondremos que no se podrá entender el mensaje:

```java
...} catch ( Exception e ) {
            msgConsola.setContenido(opAgent.getLocalName() +
                                    " El contenido del mensaje es incorrecto\n\t"
                                    + e);
   }
...
```

Esta es una estructura que deberemos tener presente siempre que tratemos mensajes **ACL**, es decir, debemos siempre dar una solución apropiada cuando tratemos los mensajes en las tareas que estén especializadas en ello.

#### Recepción del resultado de la operación

Si el `AgenteOperacion` acepta la realización de la operación hay que esperar a la respuesta con la resolución. Las opciones posibles son:

- `FAILURE`: Si la operación no se ha podido completar se recibirá la justificación que ha producido el fallo. En nuestro ejemplo si se ha producido algún problema con el objeto `Punto2D` o una excepción `ArithmeticException` al realizar la operación.
- `INFORM`: Si la operación se ha completado con éxito se recibe el resultado obtenido.

El método asociado que se ha implementado es `handleAllResultNotifications(Vector resultNotifications)`. Como en el caso anterior se activará cuando todos los `AgenteOperacion` respondan. Tendrá una estructura similar al método anterior:

```java
...
protected void handleAllResultNotifications(Vector resultNotifications) {
    Iterator it = resultNotifications.iterator();
        
    while (it.hasNext()) {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        ACLMessage msg = (ACLMessage) it.next();
        opAgent = msg.getSender();
            
        try {
            msgInfo = gsonUtil.decode(msg.getContent(), Respuesta.class);
            
            switch ( msg.getPerformative() ) {...
```

Como en el caso anterior deberemos comprobar si la **descodificación** ha sido correcta para el correcto funcionamiento.

#### Tratar mensajes fuera de secuencia

Hay que tener en cuenta que alguno de los `AgenteOperacion` no responda de forma apropiada según los mensajes esperados en el protocolo y para ello hay que implementar el método `handleOutOfSequence(.)` para evitar que la tarea no finalice de forma apropiada que pueda provocar que el `AgenteFormulario` termine **finalizando** de forma *abrupta*. 

```java
@Override
protected void handleOutOfSequence(ACLMessage msg) {
    MensajeConsola msgConsola;
        
    msgConsola = new MensajeConsola(msg.getSender().getName(),
                                       msg.getContent());
        
    agente.setMensaje(msgConsola);
}
```

#### Creación de la tarea

Esta tarea se asocia al `AgenteFormulario` y se creará una instancia cada vez que se quiera realizar una solicitud de operación a todos los `AgenteOperacion` conocidos. Esta operación se realiza en el método `enviarPunto2D(.)` del `AgenteFormulario`.

```java
public void enviarPunto2D(Punto2D punto) {
    GsonUtil<Punto2D> gsonUtil = new GsonUtil();
        
    ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
    msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
    msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
    for( AID operacion : listaAgentes )
        msg.addReceiver(operacion);
    msg.setContent(gsonUtil.encode(punto,Punto2D.class));
        
    addBehaviour(new TareaRealizarOperacionIniciador(this,msg));
}
```

Se crea el mensaje **ACL-REQUEST** que inicia el protocolo y se pasa en el constructor de la tarea. Además el mensaje se crea con un `TIME_OUT` para que el `AgenteFormulario` pueda completar la tarea en un tiempo razonable y no quede bloqueado.


### TareaRealizarOperacionParticipante

Esta tarea corresponderá al `AgenteOperacion` para resolver el rol participante del protocolo **FIPA-Request** y para ello debemos personalizar la clase `AchieveREResponder`. En el paquete `es.uja.ssmmaa.guionsesion3.tareas` definiremos esta clase:

```java
/**
 * Protocolo Request para el rol de participante. Los AgenteOperacion deciden
 * la realización de la operación sobre un Punto2D y comunican el resultado.
 * @author pedroj
 */
public class TareaRealizarOperacionParticipante extends AchieveREResponder {
    private final Consola<MensajeConsola> agente;
    private GsonUtil<Respuesta> gsonUtil;
    private Respuesta msgInfo;

    public TareaRealizarOperacionParticipante(Agent a, MessageTemplate mt) {
        super(a, mt);
        
        this.agente = (Consola) a;
        this.gsonUtil = new GsonUtil();
        this.msgInfo = new Respuesta();
    }
...
```

De la misma forma que hemos hecho para la tarea anterior, hay que implementar solo los métodos que son particulares para solucionar nuestro ejemplo. Lo más normal es *personalizar* los métodos que deben responder a la solicitud para realizar la operación y el método que comunica el resultado de la operación.

#### Responder a la solicitud de la operación

El `AgenteOperacion` tomará la decisión para realizar o no la operación. Las posibles respuestas son:

 - `NOT_UNDERSTOOD`: si el contenido del mensaje recibido es vacío.
 - `REFUSE`: se simula la toma de decisión mediante un método `aceptarOperacion()` que está configurado por un % recogido en constante `ACEPTACION`. Como norma general, en otros casos, se estudiará el contenido del mensaje y se adoptará esa decisión. También puede venir dada por el estado en que se encuentre el agente en el momento en que se recibe la solicitud.
 - `AGREE`: esta dispuesto a realizar la operación y se lleva a cabo en el siguiente método que se presentará mas adelante.

```java
@Override
protected ACLMessage handleRequest(ACLMessage request) throws RefuseException, NotUnderstoodException {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        
    if ( request.getContent() == null ) {
        msgInfo.setContenido("NO SE ENTIENDE LA OPERACION\n\t" + request);
        msgConsola.setContenido(msgInfo.toString());
        agente.setMensaje(msgConsola);
        throw new NotUnderstoodException(gsonUtil.encode(msgInfo,Respuesta.class));
    }
        
    ACLMessage agree = request.createReply();
    agree.setPerformative(ACLMessage.AGREE);
        
    if ( aceptarOperacion() ) {
        msgInfo.setContenido("Operación aceptada");
        msgConsola.setContenido(msgInfo.toString());
        agree.setContent(gsonUtil.encode(msgInfo,Respuesta.class));
    } else {
        msgInfo.setContenido("OPERACIONES SUPERADAS");
        msgConsola.setContenido(msgInfo.toString());
        agente.setMensaje(msgConsola);
        throw new RefuseException(gsonUtil.encode(msgInfo,Respuesta.class));
    }
            
    agente.setMensaje(msgConsola);
    return agree;
}
```

#### Realizar operación

Si se ha respondido afirmativamente para la realización de la operación hay que completarla. En este momento las posibilidades son las siguientes:

- `FAILURE`: No se ha recibido un `Punto2D` o se ha producido un error aritmético en la operación. En la justificación se va a distinguir el problema que se ha producido.
- `INFORM`: Se comunica el resultado de la operación realizada.

```java
@Override
protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response) 
                                                              throws FailureException {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
    ACLMessage inform = request.createReply();
    inform.setPerformative(ACLMessage.INFORM);
        
    GsonUtil<Punto2D> gsonPunto2D = new GsonUtil();
        
    try {
        Punto2D punto2D = gsonPunto2D.decode(request.getContent(), Punto2D.class);
        
        operacion(punto2D);
        msgConsola.setContenido(msgInfo.toString());
        inform.setContent(gsonUtil.encode(msgInfo,Respuesta.class));
    } catch ( ArithmeticException e ) {
        msgInfo.setContenido("OPERACION NO DEFINIDA-" + e.getMessage());
        msgConsola.setContenido(msgInfo.toString());
        agente.setMensaje(msgConsola);
        throw new FailureException(gsonUtil.encode(msgInfo,Respuesta.class));
    } catch ( Exception e ) {
        msgInfo.setContenido("No se ha recibido un Punto2D\n\t" + e.getMessage());
        msgConsola.setContenido(msgInfo.toString());
        agente.setMensaje(msgConsola);
        throw new FailureException(gsonUtil.encode(msgInfo,Respuesta.class));
    }
        
    agente.setMensaje(msgConsola);
    return inform;
}
```

Para la realización de la operación se ha definido un método `operacion(Punto2D)` que podremos revisar en el código de ejemplo.

#### Creación de la tarea

En el `AgenteOperacion` esta tarea se creará en el método `setup()` porque estará activa durante todo el ciclo de vida del agente. Es una tarea cíclica que se repetirá cada vez que se inicie el protocolo. Lo que debemos asegurarnos es crear una plantilla apropiada para que se traten los mensajes que deba tratar.

```java
...
MessageTemplate msgTemplate = MessageTemplate.and(
  		MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
  		MessageTemplate.MatchPerformative(ACLMessage.REQUEST) );
addBehaviour(new TareaRealizarOperacionParticipante(this, msgTemplate));
...
```

### 3.1.2 Protocolo FIPA-Subscribe

Para la resolución del envío de los mensajes al `AgenteConsola` utilizaremos el protocolo **FIPA-Subscribe** adaptado a nuestras necesidades quedando de esta forma:

#### TareaSubConsola

```mermaid
sequenceDiagram
	participant i as AgenteConsola 
	participant r1 as AgenteFormulario
	participant r2 as AgenteOperacion
	i->>r1: SUBSCRIBE
	Note right of r1: Solicitud de subscrip<br/>ción
	i->>r2: SUBSCRIBE 
	Note right of r2: Solicitud de subscrip<br/>ción
	alt rechazo
		r1-->>i: REFUSE (Respuesta)
	else problemas
		r1-->>i: FAILURE (Respuesta)
	else acepta
		r1-->>i: AGREE (Respuesta)
	end
	alt rechazo
		r2-->>i: REFUSE (Respuesta)
	else problemas
		r2-->>i: FAILURE (Respuesta)
	else acepta
		r2-->>i: AGREE (Respuesta)
	end
	r1-->>i: INFORM (MensajeConsola)
	Note right of r1: Informes de subscrip<br/>ción
	r2-->>i: INFORM (MensajeConsola)
	Note right of r2: Informes de subscrip<br/>ción
```

En el diagrama se muestra el intercambio de mensajes, indicando el origen y destino, de los agentes implicados. El contenido del mensaje viene representado por la clase de información que lo representa. El diagrama representa tanto al iniciador como al participante, pero para su implementación deberemos tener una tarea para el iniciador y otra para el participante.

Definimos la tarea dentro del paquete `tareas`, y podrá ser utilizada por otros tipos de agentes, vamos a necesitar una interface para poder intercambiar información entre el agente y la tarea. En este caso necesitaremos dos, una para los agentes del rol iniciador y otra para los agentes del rol participante. 

Para los agentes que realizará el rol iniciador utilizaremos la interface `Consola`. Es la misma que se ha utilizado en la **TareaRealizarOperacion** porque la información que se va a generar en la **TareaSubConsola** es la misma, es decir, un objeto de la clase `MensajeConsola`.

Para los agentes que realizarán el rol participante debemos definir una nueva interface que heredará de la interface `Consola` porque necesitaremos enviar la información que se genera en la tarea y además debemos definir nuevos métodos necesarios para solicitar información al agente para poder completar la tarea.

```java
/**
 * Interface diseñada para las tareas de los agentes que cumplen con un rol
 * de participante en un protocolo de subscripción y que pretenden enviarle
 * los mensajes que presentarán en un elemento gráfico que cumpla con las 
 * características de una consola para un agente.
 * @author pedroj
 */
public interface EnvioConsola<T> extends Consola<T> {
    public GestorSubscripciones getGestor();
    public List<T> getMensajes();
}
```

### TareaSubConsolaIniciador

Esta tarea corresponderá al `AgenteConsola` para resolver el rol iniciador del protocolo **FIPA-Subscription** y para ello debemos personalizar la clase `SubscriptionInitiator`. En el paquete `es.uja.ssmmaa.guionsesion3.tareas` definiremos esta clase:

```java
/**
 * Protocolo Subscribe para el rol del agente iniciador para la recepción de 
 * mensajes de tipo MensajeConsola que se le pasarán al agente.
 * @author pedroj
 */
public class TareaSubConsolaIniciador extends SubscriptionInitiator {
    private Consola<MensajeConsola> agente;
    private GsonUtil<Respuesta> gsonUtil;
    private GsonUtil<MensajeConsola> gsonConsola;
    private Respuesta msgInfo;
    private AID emisor;
    
    public TareaSubConsolaIniciador(Agent a, ACLMessage msg) {
        super(a, msg);
        
        this.agente = (Consola) a;
        this.gsonUtil = new GsonUtil();
        this.gsonConsola = new GsonUtil();
        this.msgInfo = null;
        this.emisor = null;
    }
...
```

Ahora debemos implementar los métodos necesarios para poder completar la tarea de subscripción. Lo que tendremos que implementar seguro es:

- Esperar la respuesta a la solicitud de suscripción
- Tratar los mensajes `INFORM` que recibamos por nuestra suscripción
- Los mensajes que no sigan la secuencia del protocolo.

#### Respuestas a la solicitud de la subscripción

Vamos a centralizar todas las posibles respuestas en un único método que tratará los mensajes para la subscripción. En el ejemplo se va a solicitar la suscripción de forma individual a los `AgenteFormulario` y `AgenteOperacion` cuando se registren en el servicio de páginas amarillas. Pero podría haberse diseñado de otra forma y solicitar las subscripciones cada cierto tiempo. En la implementación no se tendrá en cuenta aquellos agentes que no respondan en el tiempo establecido por `TIME_OUT`.

- `REFUSE`: no se ha podido completar la solicitud de subscripción con el agente participante.
- `FAILURE`: se produce un problema cuando se intenta cancelar la subscripción. Pero no tendrá mucho interés porque no se tendrá en cuenta este caso en la implementación del `AgenteConsola`.
- `AGREE`: se ha completado la subscripción de forma correcta.

El método que debemos implementar es `handleAllResponses(Vector responses)` y su estructura es similar a métodos similares en `TareaRealizarOperacionIniciador`.

```java
@Override
protected void handleAllResponses(Vector responses) {
    Iterator it = responses.iterator();
        
    while (it.hasNext()) {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        ACLMessage msg = (ACLMessage) it.next();
        emisor = msg.getSender();
            
        try {
            msgInfo = gsonUtil.decode(msg.getContent(), Respuesta.class);
            
            switch ( msg.getPerformative() ) {...
```

#### Tratar los mensajes INFORM

Estos mensajes no se tratarán inmediatamente, es decir, dependerán de las condiciones de la subscripción y la forma en que se implemente, más adelante se mostrará, la tarea encargada del envío en los agentes con el rol participante. El método a implementar es `handleInform(ACLMessage inform)`:

```java
@Override
protected void handleInform(ACLMessage inform) {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
    emisor = inform.getSender();
        
    try {
        msgConsola = gsonConsola.decode(inform.getContent(), MensajeConsola.class);
            
    } catch ( Exception e ) {
            msgConsola.setContenido(emisor.getLocalName() +
                                    " El contenido del mensaje es incorrecto\n\t"
                                    + e);
    }
        
    agente.setMensaje(msgConsola);
}
```

Nos aseguramos que se ha recibido un `MensajeConsola` y se pasará al agente para que lo presente en la ventana correspondiente.

#### Tratar mensajes fuera de secuencia

Hay que tener en cuenta que alguno de los `AgenteFormulario` o `AgenteOperacion` no responda de forma apropiada según los mensajes esperados en el protocolo y para ello hay que implementar el método `handleOutOfSequence(.)` para evitar que la tarea no finalice de forma apropiada que pueda provocar que el `AgenteConsola` termine **finalizando** de forma *abrupta*. 

```java
@Override
protected void handleOutOfSequence(ACLMessage msg) {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
    msgConsola.setContenido("Error en la tarea de Subcripción " + 
                msg.getSender().getLocalName() + "\nERROR\n" + msg);
    agente.setMensaje(msgConsola);
}
```

#### Cancelar la subscripción

No vamos a cambiar el comportamiento del método asociado para la cancelación de la subscripción pero en la implementación del `AgenteConsola` vamos a tener que invocar el método asociado cuando de detecte que uno de los agentes con los que tenemos subscripción finalice su ejecución, es decir, abandone el registro de páginas amarillas. También se solicitará la cancelación de las subscripciones del `AgenteConsola` cuando finalice su ejecución. Para ello el `AgenteConsola` debe guardar una referencia de las tareas de subscripción para poder invocar el método `cancel()` en el momento apropiado. En el `AgenteConsola` se define un método para ello:

```java
private void cancelarSubscripcion(AID agente) {
    ConsolaJFrame gui = buscarConsola(agente.getLocalName());
    gui.dispose();
    TareaSubConsolaIniciador sub = subscripciones.remove(agente.getLocalName());
    if( sub != null ) {
        MensajeConsola msg = new MensajeConsola(this.getLocalName());
        msg.setContenido("Solicitar CANCELACION subscrición de " + agente);
        setMensaje(msg);
        sub.cancel(agente, true);
    }
}
```
 
#### Crear la tarea

Como se ha comentado anteriormente, se creará una tarea cada vez que se detecte que un `AgenteFormulario` o `AgenteOperacion` se ha registrado en el servicio de páginas amarillas. En el `AgenteConsola` se define un método para ello:

```java
private void crearSubscripcion(AID agente) {
    //Creamos el mensaje para lanzar el protocolo Subscribe
    ACLMessage msg = new ACLMessage(ACLMessage.SUBSCRIBE);
    msg.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
    msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
    msg.setSender(this.getAID());
    msg.addReceiver(agente);
        
    // Añadimos la tarea de suscripción
    TareaSubConsolaIniciador sub = new TareaSubConsolaIniciador(this,msg);
    subscripciones.put(agente.getLocalName(), sub);
    addBehaviour(sub);
}
```

### TareaSubConsolaParticipante

Esta tarea corresponderá al `AgenteFormulario` y al `AgenteConsola` para resolver el rol participante del protocolo **FIPA-Subscription** y para ello debemos personalizar la clase `SubscriptionResponder`. En el paquete `es.uja.ssmmaa.guionsesion3.tareas` definiremos esta clase:

```java
/**
 * Protocolo Subcribe para el agente con el rol participante. Responderá a las
 * solicitudes de subscripción y generarán el objeto Subsctiption asociado para
 * almacenarlo en el gestor de susbscripciones y así poder enviarle los MensajeConsola
 * cuando estén disponibles en el agente.
 * @author pedroj
 */
public class TareaSubConsolaParticipante extends SubscriptionResponder {
    private Subscription suscripcionJugador;
    private EnvioConsola<MensajeConsola> agente;
    private Respuesta infMensaje;
    private GsonUtil<Respuesta> gsonUtil;
    private GestorSubscripciones gestor;
    
    public TareaSubConsolaParticipante(Agent a, MessageTemplate mt, SubscriptionManager sm) {
        super(a, mt, sm);
        
        this.agente = (EnvioConsola) a;
        this.infMensaje = new Respuesta();
        this.gsonUtil = new GsonUtil();
        this.gestor = agente.getGestor();
    }
...
```

Un elemento necesario para este tipo de tareas será el gestor para los elementos `Subscription` que se crean. Este elemento se explicará más adelante cuando se detallen las clases de utilidad que han sido necesarias definir para este ejemplo.

En este tipo de tareas debemos implementar los métodos que se encargan de:

- Responder a las solicitudes de subscripción que se soliciten.
- Responder a las solicitudes de cancelación de la subscripción que se soliciten.

#### Solicitud de subscripción

El método deberá responder apropiadamente a una solicitud de subscripción que se recibe. Las posibilidades son:

- `NOT_UNDERSTOOD`: no se entiende el contenido del mensaje. En nuestro ejemplo no se va a enviar ningún tipo de información y por tanto no se tratará este tipo de respuesta. Pero no en todos los problemas será así.
- `REFUSE`: se rechaza la solicitud de subscripción. En nuestro caso se tomará esta decisión si se produce algún problema en la tramitación de la subscripción, es decir, no se ha podido crear correctamente el elemento `Subscription` asociado a la petición.
- `AGREE`: se ha creado correctamente la subscripción.

El método que se debe implementar es `handleSubscription(ACLMessage subscription)`:

```java
@Override
protected ACLMessage handleSubscription(ACLMessage subscription) throws NotUnderstoodException, 
                                                                                RefuseException {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
    String nombreAgente = subscription.getSender().getName();
        
    try {
        // Registra la suscripción si no hay una previa
        if (!gestor.haySubscripcion(nombreAgente)) {
            suscripcionJugador = createSubscription(subscription);
            mySubscriptionManager.register(suscripcionJugador);
        }   
    } catch (Exception e) {
        infMensaje.setContenido("Error al registrar la subscripción en " 
                                    + myAgent.getLocalName());
        System.out.println(myAgent.getLocalName() + "\n\t" + infMensaje);
        throw new RefuseException(gsonUtil.encode(infMensaje, Respuesta.class));
    }
        
    // Responde afirmativamente a la suscripción
    ACLMessage agree = subscription.createReply();
    infMensaje.setContenido("Subscripción creada en " + myAgent.getLocalName());
    msgConsola.setContenido("Subscripción creada para " + nombreAgente);
    agente.setMensaje(msgConsola);
    agree.setPerformative(ACLMessage.AGREE);
    agree.setContent(gsonUtil.encode(infMensaje, Respuesta.class));
    return agree;
}
```

#### Cancelación de la subscripción

Este método será necesario para la cancelación de la subscripción de un agente. Es importante mantener bien actualizadas las subscripciones para evitar problemas o gastar tiempo innecesario en la ejecución de los agentes. Esta método puede responder de la siguiente forma:

- `FAILURE`: si se produce algún problema en la cancelación de la subscripción. En este problema, y en muchos casos, estará determinado si se ha producido algún problema a la hora de eliminar el elemento `Subscription` en el gestor de subscripciones.
- `INFORM`: la subscripción se ha cancelado correctamente.

El método que se debe implementar es `handleCancel(ACLMessage cancel)`:

```java
@Override
protected ACLMessage handleCancel(ACLMessage cancel) throws FailureException {
    MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        
    // Eliminamos la suscripción del agente jugador
    String nombreAgente = cancel.getSender().getName();
                
    try {
        suscripcionJugador = gestor.getSubscripcion(nombreAgente);
        mySubscriptionManager.deregister(suscripcionJugador);
    } catch ( Exception e ) {
        infMensaje.setContenido("Error al cancelar la subscripción para " + nombreAgente);
        System.out.println(myAgent.getLocalName() + "\n\t" + infMensaje);
        throw new FailureException(gsonUtil.encode(infMensaje, Respuesta.class));
    }
        
    // Mensaje de cancelación
    ACLMessage cancela = cancel.createReply();
    infMensaje.setContenido("Subscripción cancelada en " + myAgent.getLocalName());
    msgConsola.setContenido("Subscripción cancelada para " + nombreAgente);
    agente.setMensaje(msgConsola);
    cancela.setPerformative(ACLMessage.INFORM);
    cancela.setSender(myAgent.getAID());
    cancela.setContent(gsonUtil.encode(infMensaje, Respuesta.class));
    return cancela;
}
```

#### Creación de la tarea

Esta tarea se crea en el método `setup()` de los `AgenteFormulario` y `AgenteOperacion` porque es una tarea que debe estar activa durante todo el ciclo de vida del agente. Es una tarea cíclica que estará a la espera de la recepción del mensaje de inicio del protocolo. Debemos tener cuidado al definir el *template* de los mensajes que debe tratar esta tarea:

```java
...
// Plantilla del mensaje de suscripción
MessageTemplate plantilla;
plantilla= MessageTemplate.and(
               MessageTemplate.not(
                   MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()), 
                                      MessageTemplate.MatchSender(this.getAMS()))), 
               MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE));
addBehaviour(new TareaSubConsolaParticipante(this,plantilla,gestor));
...
```

Debemos asegurarnos que la tarea está a la espera de mensajes del protocolo `FIPA_SUBSCRIBE` pero que sean de cualquier agente presente en la plataforma excepto del agente `ams` y `df`. Eso agentes especializados de la plataforma pueden enviarnos distintos tipos de mensajes y no son los que esta tarea debe tratar.

### TareaEnvioMensaje

Esta es la tarea que debe comprobar si los `AgenteFormularia` y `AgenteOperacion` tienen subscripciones activas y mensajes que enviar. La tarea se ha definido como `TickerBehaviour`, pero lo importante que hay que entender es que en el rol participante para el protocolo **FIPA-Subscribe** debe haber una tarea que sea la encargada en revisar el gestor de las subscripciones y decidir si tiene que enviar un mensaje `INFORM` al agente asociado a esa subscripción.

```java
/**
 * Tarea que se repetirá cada intervalo de tiempo ESPERA para comprobar si hay
 * mensajes que enviar a agentes que previamente han formalizado una subscripción.
 * El tipo de mensaje que se envía es MensajeConsola.
 * @author pedroj
 */
public class TareaEnvioMensajes extends TickerBehaviour {
    private final EnvioConsola<MensajeConsola> agente;
    private final GsonUtil<MensajeConsola> gsonUtil;

    public TareaEnvioMensajes(Agent a, long period) {
        super(a, period);
        
        this.agente = (EnvioConsola) a;
        this.gsonUtil = new GsonUtil();
    }
...
```

La tarea se ejecutará cíclicamente según se especifica en la constante `ESPERA`. Cada vez que se ejecute comprobará si hay subscripciones activas y que hay MensajeConsola disponibles. Tienen que darse las dos condiciones, en otro caso no hace nada.

```java
@Override
protected void onTick() {
    GestorSubscripciones gestor = agente.getGestor();
    List<MensajeConsola> mensajes = agente.getMensajes();
        
    // Hay mensajes pendientes y subscripciones activas
    if( !gestor.isEmpty() && !mensajes.isEmpty() )
        // Para todas las subscripciones activas
        for( Subscription subscripcion : gestor.values() ) 
            enviar(subscripcion, mensajes);
    
    mensajes.clear();
}
```

A cada subscriptor se le enviará la misma lista de mensajes pendientes, es decir, en nuestro ejemplo todos los subscriptores serán `AgenteConsola` y tienen el mismo objetivo. Pero se podría haber elegido otro modo de actuar y solo enviar a un único subscriptor o haber elegido tener una única subscripción, para evitar tener más información de la necesaria.

```java
private void enviar( Subscription subscripcion, List<MensajeConsola> mensajes ) {
    for( MensajeConsola contenido : mensajes ) {
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setContent(gsonUtil.encode(contenido, MensajeConsola.class));
            
        // Enviamos el mensaje al suscriptor
        subscripcion.notify(msg);
    }
}
```

Para enviar el mensaje al agente de la subscripción utilizamos el método `notify(.)` del objeto `Subscription` asociado al agente de la subscripción. De esta forma llegará al destino apropiado.

#### Crear la tarea

Esta tarea se crea en el método `setup()` para los `AgenteFormulario` y `AgenteOperacion` porque debe estar activa durante todo el ciclo de vida del agente.

```java
...
//Añadir las tareas principales
addBehaviour(new TareaEnvioMensajes(this,ESPERA));
...
```

## 3.2 Nuevas clases de utilidad

Para la realización del ejercicio ha sido necesario definir nuevas clases de utilidad. También se han realizado algunas modificaciones de las que ya estaban definidas para el **Guión de la Sesión 2**.

- `Respuesta`: esta clase se utiliza para completar el contenido de la mayoría de los mensajes que se utilizan en los protocolos presentados. Así se puede practicar con la **codificación** y **descodificación** en el contenido de los mensajes **ACL**. Aunque esta no será la forma habitual de hacerlo, en el siguiente guión se presenta la forma más útil de hacerlo.

- `GsonUtil<T>`: esta es una clase de utilidad que se define para la **codificación** y **descodificación** para un tipo **T**.

- `GestorSubscripciones`: esta clase implementa la interface `SubscriptionManager` para poder adaptar el gestor de subscripciones a las necesidades de nuestro problema.

### 3.2.1 `GsonUtil<T>`

Esta clase utiliza la biblioteca [`gson`](https://sites.google.com/site/gson/Home) que es una implementación en Java la definición [JSON](https://www.json.org/json-en.html). Esto permitirá una **codificación** y **descodificación** de un objeto Java cualquiera a un objeto `String` de Java y viceversa. Esto es importante porque el contenido del mensaje ACL, para la implementación en Jade, es un objeto `String`.

La clase tiene definido un método para la **codificación**:

```java
public String encode(T contenido, Class<T> typeParameterClass) {
    if( contenido != null )
        return gson.toJson(contenido, typeParameterClass);
    else
        throw new NullPointerException();
}
```

Obtendremos el objeto `String` asociado al objeto del tipo `T`.

El método para la **descodificación**:

```java
public T decode(String contenido, Class<T> typeParameterClass) {
    if( contenido != null )
        return gson.fromJson(contenido, typeParameterClass);
    else
        throw new NullPointerException();
}
```

De un objeto `String` obtenemos el objeto del tipo `T` asociado.

### 3.2.2 `GestorSubscripciones`

Esta clase implementa la interface `SubscriptionManager` para dar una implementación de los métodos:

- `register(SubscriptionResponder.Subscription s)`: para registrar un objeto de `Subscription` asociado al agente que ha solicitado la subscripción para poder enviarle posteriormente los mensajes `INFORM` asociados a esa subscripción.

```java
@Override
public boolean register(SubscriptionResponder.Subscription s) throws RefuseException, NotUnderstoodException {
    // Guardamos la suscripción asociada al agente que la solita
    String nombreAgente = s.getMessage().getSender().getName();
    subscripciones.put(nombreAgente, s);
    return true;
}
```

- `deregister(SubscriptionResponder.Subscription s)`: para eliminar del gestor y finalizar con la subscripción.

```java
@Override
public boolean deregister(SubscriptionResponder.Subscription s) throws FailureException {
    // Eliminamos la suscripción asociada a un agente
    String nombreAgente = s.getMessage().getSender().getName();
    subscripciones.remove(nombreAgente);
    s.close(); // queda cerrada la suscripción
    return true;
}
```

La estructura de datos que utilicemos para almacenar los objetos `Subscription` depende de las características del problema que queremos resolver. En este ejemplo he utilizado un `Map` que tendrá como clave el nombre del agente y el objeto `Subscription` asociado como valor. No sería necesario hacerlo así para este ejemplo, no estamos discriminando entre los agentes que tienen subscripciones activas, pero puede ser apropiada para resolver otro tipo de problemas y por eso he decidido hacerlo así.

También se han añadido otros métodos, solo sería necesario implementar los dos que he puesto anteriormente, por utilidad y facilitar la implementación de otro tipo de problemas si se desea utilizar esta clase de utilidad.

## 3.3 Modificación propuesta

Se propone una modificación opcional partiendo del [proyecto](https://gitlab.com/ssmmaa/guionsesion3/-/archive/v1.2/guionsesion3-v1.2.zip) del ejemplo realizar las modificaciones necesarias para incluir el `AgenteMonitor` propuesto en el guión de la [Sesión 2](https://gitlab.com/ssmmaa/guionsesion2#27-pr%C3%A1ctica-a-realizar).


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY0ODQwMDEwMCwtMTkxOTY5MTU0MywxNT
kwNTM3NTUyXX0=
-->