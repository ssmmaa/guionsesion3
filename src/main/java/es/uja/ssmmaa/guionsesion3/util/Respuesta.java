/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.util;

/**
 *
 * @author pedroj
 */
public class Respuesta {
    private String contenido;

    public Respuesta() {
        this.contenido = null;
    }

    public Respuesta(String contenido) {
        this.contenido = contenido;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    @Override
    public String toString() {
        return "Resultado{" + "contenido=" + contenido + '}';
    }
}
