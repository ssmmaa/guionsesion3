/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final Random aleatorio = new Random();
    public static final long ESPERA = 4000; // 4 segundos
    public static final long TIME_OUT = 2000; // 2 segundos;
    public static final int NO_ENCONTRADO = -1;
    public static final int ACEPTAR = 85; // 85% de aceptación para la operación 
    public static final int PRIMERO = 0;
    public static final int SEGUNDO = 1;
    public static final int D100 = 100; // Representa un dado de 100
    
    public enum TipoServicio {
        GUI, UTILIDAD, SISTEMA; 
    }
    public static final TipoServicio[] TIPOS = TipoServicio.values();
    public enum NombreServicio {
        OPERACION, CONSOLA, FORMULARIO;
    }
    public static final NombreServicio[] SERVICIOS = NombreServicio.values();
    
    public enum Operacion {
        SUMA(25), RESTA(50), MULTIPLICACION(75), DIVISION(100);
        
        private int valor;

        private Operacion(int valor) {
            this.valor = valor;
        }
        
        /**
         * Devuelve una operación de las disponibles aleatoriamente
         * @return Operacion; una de las operaciones disponibles
         */
        public static Operacion getOperacion() {
            int tiradaDado = aleatorio.nextInt(D100);
            
            for( Operacion operacion : Operacion.values() ) {
                if( operacion.valor > tiradaDado )
                    return operacion;
            }
            
            return DIVISION;
        }
    }
    public static final Operacion[] OPERACIONES = Operacion.values();   
}
