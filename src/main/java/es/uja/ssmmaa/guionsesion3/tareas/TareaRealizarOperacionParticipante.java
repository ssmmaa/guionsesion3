/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.tareas;

import static es.uja.ssmmaa.guionsesion3.Constantes.ACEPTAR;
import static es.uja.ssmmaa.guionsesion3.Constantes.D100;
import es.uja.ssmmaa.guionsesion3.Constantes.Operacion;
import static es.uja.ssmmaa.guionsesion3.Constantes.aleatorio;
import es.uja.ssmmaa.guionsesion3.util.GsonUtil;
import es.uja.ssmmaa.guionsesion3.util.MensajeConsola;
import es.uja.ssmmaa.guionsesion3.util.Punto2D;
import es.uja.ssmmaa.guionsesion3.util.Respuesta;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

/**
 * Protocolo Request para el rol de participante. Los AgenteOperacion deciden
 * la realización de la operación sobre un Punto2D y comunican el resultado.
 * @author pedroj
 */
public class TareaRealizarOperacionParticipante extends AchieveREResponder {
    private final Consola<MensajeConsola> agente;
    private GsonUtil<Respuesta> gsonUtil;
    private Respuesta msgInfo;

    public TareaRealizarOperacionParticipante(Agent a, MessageTemplate mt) {
        super(a, mt);
        
        this.agente = (Consola) a;
        this.gsonUtil = new GsonUtil();
        this.msgInfo = new Respuesta();
    }

    /**
     * Se toma la decisión para realizar la operación solicitada. Utilizamos un
     * % ACEPTAR para decidir si se acepta o no la realización. Simula que el
     * agente puede tomar la decisión para realizar o no la acción.
     * @param request
     *          Mensaje ACL con el Punto2D.
     * @return
     *          Mensaje ACL con el resultado aceptando.
     * @throws RefuseException
     *          Mensaje ACL con el resultado del rechazo.
     * @throws NotUnderstoodException 
     *          Mensaje ACL si no hay contenido en el mensaje.
     */
    @Override
    protected ACLMessage handleRequest(ACLMessage request) throws RefuseException, NotUnderstoodException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        
        if ( request.getContent() == null ) {
            msgInfo.setContenido("NO SE ENTIENDE LA OPERACION\n\t" + request);
            msgConsola.setContenido(msgInfo.toString());
            agente.setMensaje(msgConsola);
            throw new NotUnderstoodException(gsonUtil.encode(msgInfo,Respuesta.class));
        }
        
        ACLMessage agree = request.createReply();
        agree.setPerformative(ACLMessage.AGREE);
        
        if ( aceptarOperacion() ) {
            msgInfo.setContenido("Operación aceptada");
            msgConsola.setContenido(msgInfo.toString());
            agree.setContent(gsonUtil.encode(msgInfo,Respuesta.class));
        } else {
            msgInfo.setContenido("OPERACIONES SUPERADAS");
            msgConsola.setContenido(msgInfo.toString());
            agente.setMensaje(msgConsola);
            throw new RefuseException(gsonUtil.encode(msgInfo,Respuesta.class));
        }
            
        agente.setMensaje(msgConsola);
        return agree;
    }

    /**
     * Se realiza una operación aleatoria de entre Operacion sobre el Punto2D 
     * y se envía el resultado de la operación como resultado.
     * @param request
     *          Mensaje ACL con el Punto2D.
     * @param response
     *          Mensaje ACL con la respuesta enviada en el método anterior.
     * @return
     *          Mensaje ACL con el resultado de la operación.
     * @throws FailureException 
     *          Mensaje ACL con el motivo del fallo de la operación solicitada.
     */
    @Override
    protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response) 
                                                                    throws FailureException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        ACLMessage inform = request.createReply();
        inform.setPerformative(ACLMessage.INFORM);
        
        GsonUtil<Punto2D> gsonPunto2D = new GsonUtil();
        
        try {
            Punto2D punto2D = gsonPunto2D.decode(request.getContent(), Punto2D.class);
        
            operacion(punto2D);
            msgConsola.setContenido(msgInfo.toString());
            inform.setContent(gsonUtil.encode(msgInfo,Respuesta.class));
        } catch ( ArithmeticException e ) {
            msgInfo.setContenido("OPERACION NO DEFINIDA-" + e.getMessage());
            msgConsola.setContenido(msgInfo.toString());
            agente.setMensaje(msgConsola);
            throw new FailureException(gsonUtil.encode(msgInfo,Respuesta.class));
        } catch ( Exception e ) {
            msgInfo.setContenido("No se ha recibido un Punto2D\n\t" + e.getMessage());
            msgConsola.setContenido(msgInfo.toString());
            agente.setMensaje(msgConsola);
            throw new FailureException(gsonUtil.encode(msgInfo,Respuesta.class));
        }
        
        agente.setMensaje(msgConsola);
        return inform;
    }
    
    /**
     * Simula la toma de la decisión para la realización o no de la operación.
     * @return 
     *      true si se realiza la operación y false si se rechaza.
     */
    private boolean aceptarOperacion() {
        return aleatorio.nextInt(D100) < ACEPTAR;
    }
    
    /**
     * Se realiza una operación aleatoria de entre las disponibles en Operaciones 
     * @param punto
     *          Punto2D para realizar la operación.
     * @throws ArithmeticException 
     *          Si la operación no está definida para los elementos del Punto2D.
     */
    private void operacion (Punto2D punto) throws ArithmeticException {
        double resultado;
        
        //Se realiza una operación elegida de forma aleatoria
        Operacion opSeleccionada = Operacion.getOperacion(); 
        switch ( opSeleccionada.ordinal() ) {
            case 0:
                //Suma
                resultado = punto.getX() + punto.getY();
                msgInfo.setContenido("Se ha realizado la suma de " + punto
                                      + "\n\tcon el resultado: "+ resultado);
                break;
            case 1:
                //Resta
                resultado = punto.getX() - punto.getY();
                msgInfo.setContenido("Se ha realizado la resta de " + punto
                                      + "\n\tcon el resultado: "+ resultado);
                break;
            case 2:
                //Multiplicación
                resultado = punto.getX() * punto.getY();
                msgInfo.setContenido("Se ha realizado la multiplicación de " + punto
                                      + "\n\tcon el resultado: "+ resultado);
                break;
            default:
                //División
                resultado = punto.getX() / punto.getY();
                if( Double.isFinite(resultado) || Double.isNaN(resultado) ) {
                    msgInfo.setContenido("Se ha realizado la división de " + punto
                                          + "\n\tcon el resultado: "+ resultado);
                    throw new ArithmeticException("División por 0");
                }          
        }
    }
}
