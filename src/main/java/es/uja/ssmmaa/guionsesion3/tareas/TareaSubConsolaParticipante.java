/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.tareas;

import es.uja.ssmmaa.guionsesion3.util.GestorSubscripciones;
import es.uja.ssmmaa.guionsesion3.util.GsonUtil;
import es.uja.ssmmaa.guionsesion3.util.MensajeConsola;
import es.uja.ssmmaa.guionsesion3.util.Respuesta;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionResponder;

/**
 * Protocolo Subcribe para el agente con el rol participante. Responderá a las
 * solicitudes de subscripción y generarán el objeto Subsctiption asociado para
 * almacenarlo en el gestor de susbscripciones y así poder enviarle los MensajeConsola
 * cuando estén disponibles en el agente.
 * @author pedroj
 */
public class TareaSubConsolaParticipante extends SubscriptionResponder {
    private Subscription suscripcionJugador;
    private EnvioConsola<MensajeConsola> agente;
    private Respuesta infMensaje;
    private GsonUtil<Respuesta> gsonUtil;
    private GestorSubscripciones gestor;
    
    public TareaSubConsolaParticipante(Agent a, MessageTemplate mt, SubscriptionManager sm) {
        super(a, mt, sm);
        
        this.agente = (EnvioConsola) a;
        this.infMensaje = new Respuesta();
        this.gsonUtil = new GsonUtil();
        this.gestor = agente.getGestor();
    }

    /**
     * Se cancelará la subscripión activa de un agente atendiendo a su soliciud.
     * Si hay algún problema en el objeto Subscription asociado se comunicará que
     * ha fallado la cancelación.
     * @param cancel
     *          Mensaje ACL de cancelación.
     * @return
     *          Mensaje ACL informando sobre la cancelación.
     * @throws FailureException 
     *          Mensaje ACL si se ha producido algún problema en la cancelación.
     */
    @Override
    protected ACLMessage handleCancel(ACLMessage cancel) throws FailureException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        
        // Eliminamos la suscripción del agente jugador
        String nombreAgente = cancel.getSender().getName();
                
        try {
            suscripcionJugador = gestor.getSubscripcion(nombreAgente);
            mySubscriptionManager.deregister(suscripcionJugador);
        } catch ( Exception e ) {
            infMensaje.setContenido("Error al cancelar la subscripción para " + nombreAgente);
            System.out.println(myAgent.getLocalName() + "\n\t" + infMensaje);
            throw new FailureException(gsonUtil.encode(infMensaje, Respuesta.class));
        }
        
        // Mensaje de cancelación
        ACLMessage cancela = cancel.createReply();
        infMensaje.setContenido("Subscripción cancelada en " + myAgent.getLocalName());
        msgConsola.setContenido("Subscripción cancelada para " + nombreAgente);
        agente.setMensaje(msgConsola);
        cancela.setPerformative(ACLMessage.INFORM);
        cancela.setSender(myAgent.getAID());
        cancela.setContent(gsonUtil.encode(infMensaje, Respuesta.class));
        return cancela;
    }

    /**
     * Se aceptará la subscripció si no se produce ningún problema en la generación
     * del objeto Subscription asociado al agente que la solicita.
     * @param subscription
     *          Mensaje ACL con la solicitud de subscripción.
     * @return
     *          Mensaje ACL con la aceptación de la subscripción.
     * @throws NotUnderstoodException
     *          No se trata por no analizar el contenido del mensaje subscription
     * @throws RefuseException 
     *          Mensaje ACL con el rechazo si no se ha podico completar la subscripción.
     */
    @Override
    protected ACLMessage handleSubscription(ACLMessage subscription) throws NotUnderstoodException, 
                                                                                RefuseException {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        String nombreAgente = subscription.getSender().getName();
        
        try {
            // Registra la suscripción si no hay una previa
            if (!gestor.haySubscripcion(nombreAgente)) {
                suscripcionJugador = createSubscription(subscription);
                mySubscriptionManager.register(suscripcionJugador);
            }   
        } catch (Exception e) {
            infMensaje.setContenido("Error al registrar la subscripción en " 
                                    + myAgent.getLocalName());
            System.out.println(myAgent.getLocalName() + "\n\t" + infMensaje);
            throw new RefuseException(gsonUtil.encode(infMensaje, Respuesta.class));
        }
        
        // Responde afirmativamente a la suscripción
        ACLMessage agree = subscription.createReply();
        infMensaje.setContenido("Subscripción creada en " + myAgent.getLocalName());
        msgConsola.setContenido("Subscripción creada para " + nombreAgente);
        agente.setMensaje(msgConsola);
        agree.setPerformative(ACLMessage.AGREE);
        agree.setContent(gsonUtil.encode(infMensaje, Respuesta.class));
        return agree;
    }
}
