/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.tareas;

import es.uja.ssmmaa.guionsesion3.util.GsonUtil;
import es.uja.ssmmaa.guionsesion3.util.MensajeConsola;
import es.uja.ssmmaa.guionsesion3.util.Respuesta;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionInitiator;
import java.util.Iterator;
import java.util.Vector;

/**
 * Protocolo Subscribe para el rol del agente iniciador para la recepción de 
 * mensajes de tipo MensajeConsola que se le pasarán al agente.
 * @author pedroj
 */
public class TareaSubConsolaIniciador extends SubscriptionInitiator {
    private Consola<MensajeConsola> agente;
    private GsonUtil<Respuesta> gsonUtil;
    private GsonUtil<MensajeConsola> gsonConsola;
    private Respuesta msgInfo;
    private AID emisor;
    
    public TareaSubConsolaIniciador(Agent a, ACLMessage msg) {
        super(a, msg);
        
        this.agente = (Consola) a;
        this.gsonUtil = new GsonUtil();
        this.gsonConsola = new GsonUtil();
        this.msgInfo = null;
        this.emisor = null;
    }

    /**
     * Resultado de la solicitud de subscripción enviada a los agentes
     * participantes del protocolo Subscribe. Se genera un MensajeConsola para
     * el agente.
     * @param responses 
     *          Mensaje ACL con el resultado a la solicitud de subscripción.
     */
    @Override
    protected void handleAllResponses(Vector responses) {
        Iterator it = responses.iterator();
        
        while (it.hasNext()) {
            MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
            ACLMessage msg = (ACLMessage) it.next();
            emisor = msg.getSender();
            
            try {
                msgInfo = gsonUtil.decode(msg.getContent(), Respuesta.class);
            
                switch ( msg.getPerformative() ) {
                    case ACLMessage.REFUSE:
                    case ACLMessage.FAILURE:
                    case ACLMessage.AGREE:
                        msgConsola.setContenido(msgInfo.toString());
                        break;
                    default:
                        msgConsola.setContenido(emisor.getLocalName() +
                                           " envia un mensaje desconocido\n\t" +
                                            msg.getContent());
                        break;
                }
            } catch ( Exception e ) {
                msgConsola.setContenido(emisor.getLocalName() +
                                          " El contenido del mensaje es incorrecto\n\t"
                                          + e);
            }
            
            agente.setMensaje(msgConsola);
        }
    }

    /**
     * Recepción de los MensajeConsola de los agentes que aceptaron la subscripción
     * cuando tienen alguno disponible y se le pasará al agente.
     * @param inform 
     *          Mensaje ACL que tiene el MensajeConsola.
     */
    @Override
    protected void handleInform(ACLMessage inform) {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        emisor = inform.getSender();
        
        try {
            msgConsola = gsonConsola.decode(inform.getContent(), MensajeConsola.class);
            
        } catch ( Exception e ) {
                msgConsola.setContenido(emisor.getLocalName() +
                                          " El contenido del mensaje es incorrecto\n\t"
                                          + e);
        }
        
        agente.setMensaje(msgConsola);
    }
    
    /**
     * Se tratará cualquier mensaje que no llegue en la secuencia esperada para
     * el protocolo Subscribe.
     * @param msg 
     *          Mensaje ACL que ha llegado fuera de la secuencia del protocolo.
     */
    @Override
    protected void handleOutOfSequence(ACLMessage msg) {
        MensajeConsola msgConsola = new MensajeConsola(myAgent.getLocalName());
        msgConsola.setContenido("Error en la tarea de Subcripción " + 
                msg.getSender().getLocalName() + "\nERROR\n" + msg);
        agente.setMensaje(msgConsola);
    }   
}
