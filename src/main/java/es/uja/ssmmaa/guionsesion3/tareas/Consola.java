/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.tareas;

/**
 * Interface definida para las tareas que estén diseñadas en generar mensajes
 * que se presenten posteriormente en una ventana que cumpla con las cracterísticas
 * de una consola para un agente.
 * @author pedroj
 */
public interface Consola<T> {
    public void setMensaje(T msg);
}
