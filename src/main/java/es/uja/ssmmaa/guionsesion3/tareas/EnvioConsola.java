/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.tareas;

import es.uja.ssmmaa.guionsesion3.util.GestorSubscripciones;
import java.util.List;

/**
 * Interface diseñada para las tareas de los agentes que cumplen con un rol
 * de participante en un protocolo de subscripción y que pretenden enviarle
 * los mensajes que presentarán en un elemento gráfico que cumpla con las 
 * características de una consola para un agente.
 * @author pedroj
 */
public interface EnvioConsola<T> extends Consola<T> {
    public GestorSubscripciones getGestor();
    public List<T> getMensajes();
}
