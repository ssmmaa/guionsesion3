/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.agentes;

import static es.uja.ssmmaa.guionsesion3.Constantes.ESPERA;
import static es.uja.ssmmaa.guionsesion3.Constantes.NO_ENCONTRADO;
import static es.uja.ssmmaa.guionsesion3.Constantes.NombreServicio.CONSOLA;
import static es.uja.ssmmaa.guionsesion3.Constantes.TipoServicio.SISTEMA;
import es.uja.ssmmaa.guionsesion3.gui.ConsolaJFrame;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import java.util.ArrayList;
import es.uja.ssmmaa.guionsesion3.Constantes.NombreServicio;
import static es.uja.ssmmaa.guionsesion3.Constantes.TIME_OUT;
import static es.uja.ssmmaa.guionsesion3.Constantes.TipoServicio.GUI;
import es.uja.ssmmaa.guionsesion3.tareas.SubscripcionDF;
import es.uja.ssmmaa.guionsesion3.tareas.TareaSubConsolaIniciador;
import es.uja.ssmmaa.guionsesion3.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.guionsesion3.util.MensajeConsola;
import jade.core.AID;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import es.uja.ssmmaa.guionsesion3.tareas.Consola;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteConsola extends Agent implements SubscripcionDF, Consola<MensajeConsola> {
    //Variables del agente
    private ArrayList<ConsolaJFrame> myGui;
    private HashMap<String,TareaSubConsolaIniciador> subscripciones;
    private HashMap<NombreServicio,ArrayList<AID>> agentes;
    
    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       myGui = new ArrayList();
       subscripciones = new HashMap();
       agentes = new HashMap();
       
       //Configuración del GUI
       
       //Registro del agente en las Páginas Amarrillas
       DFAgentDescription dfd = new DFAgentDescription();
       dfd.setName(getAID());
       ServiceDescription sd = new ServiceDescription();
       sd.setType(SISTEMA.name());
       sd.setName(CONSOLA.name());
       dfd.addServices(sd);
       try {
           DFService.register(this, dfd);
       } catch (FIPAException fe) {
            fe.printStackTrace();
       }
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       //Añadir las tareas principales
       
       //Suscripción al servicio de páginas amarillas
       //Para localiar a los agentes operación y consola
       DFAgentDescription template = new DFAgentDescription();
       ServiceDescription templateSd = new ServiceDescription();
       templateSd.setType(GUI.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSubscripcionDF(this,template));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
       try {
            DFService.deregister(this);
	}
            catch (FIPAException fe) {
            fe.printStackTrace();
	}

        try {
            //Liberación de recursos, incluido el GUI
            liberarRecursos();
        } catch (InterruptedException ex) {
            Logger.getLogger(AgenteConsola.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    /**
     * Buscamos la gui asociada a un agente y si no tiene ninguna se
     * crea una nueva.
     * @param nombreAgente
     * @return 
     *      La gui asociada al agentes
     */
    private ConsolaJFrame buscarConsola(String nombreAgente) {
        ConsolaJFrame gui = null;
        Iterator it = myGui.iterator();
        
        // Buscamos si tiene ya una ventana asociada
        while( it.hasNext() && (gui == null) ) {
            gui = (ConsolaJFrame) it.next();
            if( !gui.getNombreAgente().equals(nombreAgente) )
                gui = null;
        }
        
        // Si no creamos una nueva ventana
        if( gui == null ) {
            gui = new ConsolaJFrame(nombreAgente, this.getName());
            myGui.add(gui);
        }
                    
        return gui;
    }
    
    private void liberarRecursos() throws InterruptedException {
        MensajeConsola msg = new MensajeConsola(this.getLocalName(),
                                    "Finaliza en pocos segundos");
        setMensaje(msg);
        
        // Cancelamos las subscripciones
        for( ArrayList<AID> lista : agentes.values() )
            for( AID agente : lista )
                cancelarSubscripcion(agente);
       
        // Esperamos un tiempo para mostrar los mensajes finales
        TimeUnit.MILLISECONDS.sleep(ESPERA);
        
        //Se eliminan la consola del agente
        ConsolaJFrame gui = buscarConsola(this.getLocalName());
        gui.dispose();
    }
    
    private void crearSubscripcion(AID agente) {
        //Creamos el mensaje para lanzar el protocolo Subscribe
        ACLMessage msg = new ACLMessage(ACLMessage.SUBSCRIBE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        msg.setSender(this.getAID());
        msg.addReceiver(agente);
        
        // Añadimos la tarea de suscripción
        TareaSubConsolaIniciador sub = new TareaSubConsolaIniciador(this,msg);
        subscripciones.put(agente.getLocalName(), sub);
        addBehaviour(sub);
    }
    
    private void cancelarSubscripcion(AID agente) {
        ConsolaJFrame gui = buscarConsola(agente.getLocalName());
        gui.dispose();
        TareaSubConsolaIniciador sub = subscripciones.remove(agente.getLocalName());
        if( sub != null ) {
            MensajeConsola msg = new MensajeConsola(this.getLocalName());
            msg.setContenido("Solicitar CANCELACION subscrición de " + agente);
            setMensaje(msg);
            sub.cancel(agente, true);
        }
    }

    @Override
    public void addAgent(AID agente, NombreServicio servicio) {
        if( !agentes.containsKey(servicio) )
            agentes.put(servicio, new ArrayList());
        
        if( agentes.get(servicio).indexOf(agente) == NO_ENCONTRADO )
            agentes.get(servicio).add(agente);
        
        crearSubscripcion(agente);
    }

    @Override
    public boolean removeAgent(AID agente, NombreServicio servicio) {
        boolean resultado = false;
        
        if( agentes.containsKey(servicio) ) {
            resultado = agentes.get(servicio).remove(agente);
            
            cancelarSubscripcion(agente);
        }
        
        return resultado;
    }

    @Override
    public void setMensaje(MensajeConsola msg) {
        ConsolaJFrame gui;
        
        gui = buscarConsola(msg.getNombreAgente());
        gui.presentarSalida(msg);
    }
}
