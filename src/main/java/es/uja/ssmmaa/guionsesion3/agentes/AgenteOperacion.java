/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.agentes;

import static es.uja.ssmmaa.guionsesion3.Constantes.ESPERA;
import static es.uja.ssmmaa.guionsesion3.Constantes.NombreServicio.OPERACION;
import static es.uja.ssmmaa.guionsesion3.Constantes.TipoServicio.GUI;
import static es.uja.ssmmaa.guionsesion3.Constantes.TipoServicio.UTILIDAD;
import es.uja.ssmmaa.guionsesion3.tareas.TareaRealizarOperacionParticipante;
import es.uja.ssmmaa.guionsesion3.tareas.TareaSubConsolaParticipante;
import es.uja.ssmmaa.guionsesion3.util.GestorSubscripciones;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import es.uja.ssmmaa.guionsesion3.util.MensajeConsola;
import es.uja.ssmmaa.guionsesion3.tareas.EnvioConsola;
import es.uja.ssmmaa.guionsesion3.tareas.TareaEnvioMensajes;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteOperacion extends Agent implements EnvioConsola<MensajeConsola> { 
    //Variables del agente
    private GestorSubscripciones gestor;
    private ArrayList<MensajeConsola> mensajesPendientes;

    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       gestor = new GestorSubscripciones();
       mensajesPendientes = new ArrayList();
       
       //Configuración del GUI
       
       //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
	ServiceDescription sd = new ServiceDescription();
	sd.setType(UTILIDAD.name());
	sd.setName(OPERACION.name());
	dfd.addServices(sd);
        sd = new ServiceDescription();
	sd.setType(GUI.name());
	sd.setName(OPERACION.name());
	dfd.addServices(sd);
	try {
            DFService.register(this, dfd);
	}
	catch (FIPAException fe) {
            fe.printStackTrace();
	}
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       
       //Añadir las tareas principales
       addBehaviour(new TareaEnvioMensajes(this,ESPERA));
       
       MessageTemplate msgTemplate = MessageTemplate.and(
  		MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
  		MessageTemplate.MatchPerformative(ACLMessage.REQUEST) );
       addBehaviour(new TareaRealizarOperacionParticipante(this, msgTemplate));
       
       // Plantilla del mensaje de suscripción
       MessageTemplate plantilla;
       plantilla= MessageTemplate.and(
                    MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()), 
                                           MessageTemplate.MatchSender(this.getAMS()))), 
                    MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE));
       addBehaviour(new TareaSubConsolaParticipante(this,plantilla,gestor));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
        try {
            DFService.deregister(this);
	}
            catch (FIPAException fe) {
            fe.printStackTrace();
	}
       
       //Liberación de recursos, incluido el GUI
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    //Métodos de trabajo del agente
    @Override
    public GestorSubscripciones getGestor() {
        return gestor;
    }

    @Override
    public void setMensaje(MensajeConsola msg) {
        mensajesPendientes.add(msg);
    }
    
    @Override
    public ArrayList<MensajeConsola> getMensajes() {
        return mensajesPendientes;
    }
}
