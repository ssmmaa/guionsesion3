/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion3.agentes;

import static es.uja.ssmmaa.guionsesion3.Constantes.ESPERA;
import static es.uja.ssmmaa.guionsesion3.Constantes.NO_ENCONTRADO;
import es.uja.ssmmaa.guionsesion3.Constantes.NombreServicio;
import static es.uja.ssmmaa.guionsesion3.Constantes.NombreServicio.FORMULARIO;
import static es.uja.ssmmaa.guionsesion3.Constantes.TIME_OUT;
import static es.uja.ssmmaa.guionsesion3.Constantes.TipoServicio.GUI;
import static es.uja.ssmmaa.guionsesion3.Constantes.TipoServicio.UTILIDAD;
import es.uja.ssmmaa.guionsesion3.gui.FormularioJFrame;
import es.uja.ssmmaa.guionsesion3.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.guionsesion3.util.Punto2D;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import java.util.ArrayList;
import es.uja.ssmmaa.guionsesion3.tareas.TareaRealizarOperacionIniciador;
import es.uja.ssmmaa.guionsesion3.util.GsonUtil;
import es.uja.ssmmaa.guionsesion3.util.MensajeConsola;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import java.util.Date;
import es.uja.ssmmaa.guionsesion3.tareas.SubscripcionDF;
import es.uja.ssmmaa.guionsesion3.tareas.TareaSubConsolaParticipante;
import es.uja.ssmmaa.guionsesion3.util.GestorSubscripciones;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.lang.acl.MessageTemplate;
import es.uja.ssmmaa.guionsesion3.tareas.EnvioConsola;
import es.uja.ssmmaa.guionsesion3.tareas.TareaEnvioMensajes;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteFormulario extends Agent implements SubscripcionDF, 
                                                EnvioConsola<MensajeConsola> {
    //Variables del agente
    private FormularioJFrame myGui;
    private ArrayList<AID> listaAgentes;
    private GestorSubscripciones gestor;
    private ArrayList<MensajeConsola> mensajesPendientes;

    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       mensajesPendientes = new ArrayList();
       listaAgentes = new ArrayList();
       gestor = new GestorSubscripciones();
       
       //Configuración del GUI
       myGui = new FormularioJFrame(this);
       myGui.setVisible(true);
       
       //Registro del agente en las Páginas Amarrillas
       DFAgentDescription dfd = new DFAgentDescription();
       dfd.setName(getAID());
       ServiceDescription sd = new ServiceDescription();
       sd.setType(GUI.name());
       sd.setName(FORMULARIO.name());
       dfd.addServices(sd);
       try {
           DFService.register(this, dfd);
       } catch (FIPAException fe) {
            fe.printStackTrace();
       }
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       //Añadir las tareas principales
       addBehaviour(new TareaEnvioMensajes(this,ESPERA));
       
       // Plantilla del mensaje de suscripción
       MessageTemplate plantilla;
       plantilla= MessageTemplate.and(
                    MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()), 
                                           MessageTemplate.MatchSender(this.getAMS()))), 
                    MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE));
       addBehaviour(new TareaSubConsolaParticipante(this,plantilla,gestor));
       
       //Suscripción al servicio de páginas amarillas
       //Para localiar a los agentes operación y consola
       DFAgentDescription template = new DFAgentDescription();
       ServiceDescription templateSd = new ServiceDescription();
       templateSd.setType(UTILIDAD.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSubscripcionDF(this,template));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
       try {
            DFService.deregister(this);
	}
            catch (FIPAException fe) {
            fe.printStackTrace();
	}
       
       //Liberación de recursos, incluido el GUI
       myGui.dispose();
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    //Métodos de trabajo del agente
    @Override
    public void addAgent(AID agente, NombreServicio servicio) {
        if( listaAgentes.indexOf(agente) == NO_ENCONTRADO )
            listaAgentes.add(agente);
        
        if ( !listaAgentes.isEmpty() )
            myGui.activarEnviar(true);
    }

    @Override
    public boolean removeAgent(AID agente, NombreServicio servicio) {
        boolean resultado = listaAgentes.remove(agente);
        
        if ( listaAgentes.isEmpty() )
            myGui.activarEnviar(false);
        
        return resultado;
    }

    public void enviarPunto2D(Punto2D punto) {
        GsonUtil<Punto2D> gsonUtil = new GsonUtil();
        
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        for( AID operacion : listaAgentes )
            msg.addReceiver(operacion);
        msg.setContent(gsonUtil.encode(punto,Punto2D.class));
        
        addBehaviour(new TareaRealizarOperacionIniciador(this,msg));
    }

    @Override
    public GestorSubscripciones getGestor() {
        return gestor;
    }

    @Override
    public void setMensaje(MensajeConsola msg) {
        mensajesPendientes.add(msg);
    }

    @Override
    public ArrayList<MensajeConsola> getMensajes() {
        return mensajesPendientes;
    }
}
